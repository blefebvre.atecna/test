# UpStreamPayApp

## Requirements
- OpenJDK 11
- Lombok plugin installed in your IDE

## Context

Application is by default exposed on port 8080, you can access it via http://localhost:8080

## Composition

The project is prepackaged with following libraries :

- Spring Rest framework for API support
- OpenAPI for api documentation with Swagger-UI
- H2 database with Hibernate + JPA

## Data tests

Go to swagger url : http://localhost:8080/swagger-ui/index.html?url=/v3/api-docs/#/

1) Call this endpoint : POST /transactions

with body :

```
{
"amount": 54.80,
"paymentMethod": "CREDIT_CARD",
"orders": [
    {
    "name": "Gant de ski",
    "quantity": 4,
    "price": 10
    },
    {
    "name": "Bonnet en laine",
    "quantity": 1,
    "price": 14.80
    }
]
}
```

Get id in header location of response

2) Call this endpoint with transaction id : PUT /transactions/{transactionId}/status

with body :

```
{
  "status": "AUTHORIZED"
}
```

3) Call again the same endpoint with body :

```
{
  "status": "CAPTURED"
}
```

4) Call this endpoint : POST /transactions

with body :

```
{
"amount": 208,
"paymentMethod": "PAYPAL",
"orders": [
    {
    "name": "Vélo",
    "quantity": 1,
    "price": 208
    }
]
}
```

5) Call this endpoint  GET /transactions with void body
