package com.upstreampay.app.api;

import com.upstreampay.app.commons.UseCaseResponse;
import com.upstreampay.app.domains.transaction.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@RestController
@Tag(name = "Transaction")
public class TransactionApi {

    @Autowired
    private CreateTransactionUseCase createTransactionUseCase;

    @Autowired
    private UpdateTransactionStatusUseCase updateTransactionStatusUseCase;

    @Autowired
    private GetAllTransactionUseCase findAllFolderUseCase;

    @Operation(
            summary = "Create transaction",
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "The transaction has been created"),
                    @ApiResponse(
                            responseCode = "400",
                            content = @Content(mediaType = "application/json"),
                            description = "The transaction is malformed")})
    @PostMapping("/transactions")
    public ResponseEntity<URI> createTransaction(@RequestBody TransactionDto transactionDto) {
        UseCaseResponse<String> response = createTransactionUseCase.call(transactionDto);

        if (response.isSuccess()) {
            return ResponseEntity.created(URI.create("/transactions/" + response.getResult())).build();
        }

        throw response.exception();
    }

    @Operation(
            summary = "Update transaction status",
            responses = {
                    @ApiResponse(
                            responseCode = "204",
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
                            description = "The transaction status has been updated"),
                    @ApiResponse(
                            responseCode = "400",
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
                            description = "The transaction dto is malformed"),
                    @ApiResponse(
                            responseCode = "404",
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
                            description = "Transaction not found")})
    @PutMapping("/transactions/{transactionId}/status")
    public ResponseEntity<String> updateTransactionStatus(@PathVariable UUID transactionId, @RequestBody UpdateTransactionStatusDto dto) {
        var response = updateTransactionStatusUseCase.call(transactionId, dto);
        if (response.isSuccess()) {
            return ResponseEntity.noContent().build();
        }

        throw response.exception();
    }


    @Operation(
            summary = "Get all transactions",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            content = @Content(mediaType = "application/json"),
                            description = "All transactions has been collected")})
    @GetMapping("/transactions")
    public ResponseEntity<List<TransactionDto>> getAllTransactions() {
        var response = findAllFolderUseCase.call();
        if (response.isSuccess()) {
            return ResponseEntity.ok(response.getResult());
        }

        throw response.exception();
    }
}
