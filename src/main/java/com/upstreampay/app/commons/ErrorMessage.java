package com.upstreampay.app.commons;

public final class ErrorMessage {

    public static final String FORBIDDEN_ON_USER = "resource forbidden for current user";
    public static final String MUST_NOT_BE_NULL = "must not be null";
    public static final String NOT_FOUND = "not found";
}