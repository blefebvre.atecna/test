package com.upstreampay.app.commons;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Validator;
import java.util.List;


@Service
@Transactional
public class UseCase<T> {

    @Autowired
    protected ModelMapper modelMapper;

    @Autowired
    protected Validator validator;


    public UseCaseResponse<T> getResponse() {
        return UseCaseResponse.<T>builder().build();
    }

    public UseCaseResponse<List<T>> getListResponse() {
        return UseCaseResponse.<List<T>>builder().build();
    }

}
