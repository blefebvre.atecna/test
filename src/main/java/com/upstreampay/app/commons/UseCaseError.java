package com.upstreampay.app.commons;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UseCaseError {
    private String message;
    private String field;

    public boolean is(String entry) {
        return String.format("%s %s", field, message).equals(entry);
    }
}
