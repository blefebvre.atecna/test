package com.upstreampay.app.commons;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class UseCaseException extends Error {

	private static final long serialVersionUID = 1L;
	private int httpCode;

    public UseCaseException(String message) {
        super(message);
    }
}
