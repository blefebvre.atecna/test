package com.upstreampay.app.commons;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UseCaseHttpError {
    private List<UseCaseError> errors;
}
