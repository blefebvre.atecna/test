package com.upstreampay.app.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UseCaseResponse<T> {

    @Builder.Default
    private List<UseCaseError> errors = new ArrayList<>();

    @Builder.Default
    private int errorCode = 200;

    private T result;

    private static ObjectMapper objectMapper = new ObjectMapper();

    public boolean isSuccess() {
        return errors.isEmpty();
    }

    public boolean isFailure() {
        return !isSuccess();
    }

    public UseCaseException exception() {
        try {
            var httpError = UseCaseHttpError.builder().errors(errors).build();
            var message = objectMapper.writeValueAsString(httpError);

            var exc = new UseCaseException(message);
            exc.setHttpCode(errorCode);
            return exc;
        } catch (JsonProcessingException ex) {
            return new UseCaseException(ex.getMessage());
        }
    }

    public static <T> UseCaseResponse<T> withErrorAndCode(String field, String message, int httpCode) {
        return UseCaseResponse.<T>builder()
                .errorCode(httpCode)
                .errors(List.of(UseCaseError.builder().field(field).message(message).build()))
                .build();
    }

    public static <T> UseCaseResponse<T> withCode(int httpCode) {
        return UseCaseResponse.<T>builder()
                .errorCode(httpCode)
                .build();
    }

    public UseCaseResponse<T> addError(ConstraintViolation<?> constraintViolation) {
        var error = UseCaseError.builder()
                .field(constraintViolation.getPropertyPath().toString())
                .message(constraintViolation.getMessage())
                .build();
        this.getErrors().add(error);
        setErrorCode(400);
        return this;
    }

    public UseCaseResponse<T> addError(UseCaseError error) {
        this.getErrors().add(error);
        setErrorCode(400);
        return this;
    }

    public static <T> UseCaseResponse<T> createdResponse() {
        return withCode(HttpStatus.CREATED.value());
    }

    public static <T> UseCaseResponse<T> okResponse(T response) {
        return UseCaseResponse.<T>builder()
                .errorCode(HttpStatus.OK.value())
                .result(response)
                .build();
    }

    public static UseCaseResponse<String> noContentResponse() {
        return withCode(HttpStatus.NO_CONTENT.value());
    }

    public static <T> UseCaseResponse<T> badRequestResponse() {
        return withCode(HttpStatus.BAD_REQUEST.value());
    }

    public static <T> UseCaseResponse<T> badRequestResponse(String field, String message) {
        return withErrorAndCode(field, message, HttpStatus.BAD_REQUEST.value());

    }

    public static <T> UseCaseResponse<T> notFoundResponse(String field, String message) {
        return withErrorAndCode(field, message, HttpStatus.NOT_FOUND.value());
    }
}