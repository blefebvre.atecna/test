package com.upstreampay.app.domains.transaction;

import com.upstreampay.app.commons.UseCase;
import com.upstreampay.app.commons.UseCaseResponse;
import com.upstreampay.app.persistence.order.Order;
import com.upstreampay.app.persistence.order.OrderService;
import com.upstreampay.app.persistence.transaction.Transaction;
import com.upstreampay.app.persistence.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateTransactionUseCase extends UseCase<String> {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private OrderService orderService;

    public UseCaseResponse<String> call(TransactionDto dto) {
        var response = getResponse();

        validator.validate(dto).forEach(response::addError);
        if (response.isFailure()) return response;

        Transaction transaction = modelMapper.map(dto, Transaction.class);

        for(Order order : transaction.getOrders()){
            order.setTransaction(transaction);
            orderService.save(order);
        }

        response.setResult(transactionService.save(transaction).getId().toString());
        return response;
    }
}
