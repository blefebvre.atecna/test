package com.upstreampay.app.domains.transaction;

import com.upstreampay.app.commons.UseCase;
import com.upstreampay.app.commons.UseCaseResponse;
import com.upstreampay.app.persistence.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class GetAllTransactionUseCase extends UseCase<TransactionDto> {

    @Autowired
    private TransactionService transactionService;

    public UseCaseResponse<List<TransactionDto>> call() {
        var response = getListResponse();

        var folderList = transactionService.findAll()
               .stream()
               .map(transaction -> modelMapper.map(transaction, TransactionDto.class))
               .collect(Collectors.toList());

        response.setResult(folderList);
        return response;
    }
}