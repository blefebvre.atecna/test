package com.upstreampay.app.domains.transaction;

import com.upstreampay.app.persistence.transaction.TransactionStatus;
import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateTransactionStatusDto {

    @NotNull
    private TransactionStatus status;

}
