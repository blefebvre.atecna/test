package com.upstreampay.app.domains.transaction;

import com.upstreampay.app.commons.ErrorMessage;
import com.upstreampay.app.commons.UseCase;
import com.upstreampay.app.commons.UseCaseResponse;
import com.upstreampay.app.persistence.transaction.TransactionService;
import com.upstreampay.app.persistence.transaction.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/***
 * Sequence of status
 * NEW -> AUTHORIZED -> CAPTURE
 */

@Service
public class UpdateTransactionStatusUseCase extends UseCase<Void> {

    @Autowired
    private TransactionService transactionService;

    public UseCaseResponse<Void> call(UUID transactionId, UpdateTransactionStatusDto dto) {
        var response = getResponse();

        validator.validate(dto).forEach(response::addError);
        if (response.isFailure()) return response;

        var optTransaction = transactionService.findById(transactionId);
        if(optTransaction.isEmpty()){
            return UseCaseResponse.notFoundResponse("transactionId", ErrorMessage.NOT_FOUND);
        }
        var transaction = optTransaction.get();

        if(transaction.getStatus() == TransactionStatus.CAPTURED ||
                (dto.getStatus() == TransactionStatus.CAPTURED && !(transaction.getStatus() == TransactionStatus.AUTHORIZED))){
            return UseCaseResponse.badRequestResponse("action", ErrorMessage.FORBIDDEN_ON_USER);
        }

        modelMapper.map(dto, transaction);
        transactionService.save(transaction);

        return response;
    }
}