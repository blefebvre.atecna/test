package com.upstreampay.app.persistence.order;

import com.upstreampay.app.persistence.transaction.Transaction;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "id", strategy = "uuid4")
    private UUID id;

	@Column(nullable = false)
	private String name;

    @Column(nullable = false)
    @Min(0)
    private int quantity;

    @Column(nullable = false)
    @Min(0)
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "transaction_id")
    private Transaction transaction;

}
