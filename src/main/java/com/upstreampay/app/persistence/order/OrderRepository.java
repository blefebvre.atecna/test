package com.upstreampay.app.persistence.order;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;


public interface OrderRepository extends CrudRepository<Order, UUID> {
}
