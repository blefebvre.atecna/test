package com.upstreampay.app.persistence.transaction;

public enum PaymentMethod {
    CREDIT_CARD,
    GIFT_CARD,
    PAYPAL
}
