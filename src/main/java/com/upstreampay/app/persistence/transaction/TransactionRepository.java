package com.upstreampay.app.persistence.transaction;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;


public interface TransactionRepository extends CrudRepository<Transaction, UUID> {
}
