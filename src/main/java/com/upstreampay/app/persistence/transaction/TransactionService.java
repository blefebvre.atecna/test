package com.upstreampay.app.persistence.transaction;

import com.google.common.collect.ImmutableList;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class TransactionService {

    private final TransactionRepository repository;

    public Transaction save(Transaction folder) {
        return repository.save(folder);
    }

    public List<Transaction> findAll() {
        return ImmutableList.copyOf(repository.findAll());
    }

    public Optional<Transaction> findById(UUID folderId) {
        return repository.findById(folderId);
    }

}
