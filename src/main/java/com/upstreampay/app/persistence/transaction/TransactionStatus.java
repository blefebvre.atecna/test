package com.upstreampay.app.persistence.transaction;

public enum TransactionStatus {
    NEW,
    AUTHORIZED,
    CAPTURED
}
