package com.upstreampay.app.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upstreampay.app.commons.UseCaseResponse;
import com.upstreampay.app.domains.order.OrderDto;
import com.upstreampay.app.domains.transaction.*;
import com.upstreampay.app.persistence.transaction.PaymentMethod;
import com.upstreampay.app.persistence.transaction.TransactionStatus;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TransactionApi.class)
@AutoConfigureMockMvc
class TransactionApiTest {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CreateTransactionUseCase createTransactionUseCase;

    @MockBean
    private UpdateTransactionStatusUseCase updateTransactionStatusUseCase;

    @MockBean
    private GetAllTransactionUseCase getAllTransactionUseCase;

    @Test
    void createTransaction_success() throws Exception {
        var transactionDto = TransactionDto.builder()
                .amount(BigDecimal.valueOf(4.50))
                .paymentMethod(PaymentMethod.CREDIT_CARD)
                .orders(List.of(OrderDto.builder()
                                .name("Crayon")
                                .price(BigDecimal.valueOf(1))
                                .quantity(3)
                                .build(),
                        OrderDto.builder()
                                .name("Gomme")
                                .price(BigDecimal.valueOf(1.50))
                                .quantity(1)
                                .build()
                ))
                .build();

        var transactionIdString = UUID.randomUUID().toString();
        var response = UseCaseResponse.okResponse(transactionIdString);

        when(createTransactionUseCase.call(transactionDto)).thenReturn(response);

        mvc.perform(post("/transactions")
                .content(objectMapper.writeValueAsBytes(transactionDto))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(status().isCreated())
                .andExpect(header().exists("Location"))
                .andExpect(header().string("Location", org.hamcrest.Matchers.containsString("/transactions/" + transactionIdString)))
                .andReturn();
    }

    @Test
    void updateTransactionStatus_success() throws Exception {
        var dto = UpdateTransactionStatusDto.builder()
                .status(TransactionStatus.AUTHORIZED)
                .build();
        var transactionId = UUID.randomUUID();

        var response = UseCaseResponse.<Void>builder().build();

        when(updateTransactionStatusUseCase.call(transactionId, dto)).thenReturn(response);

        mvc.perform(put("/transactions/{transactionId}/status",  transactionId)
                .content(objectMapper.writeValueAsBytes(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    void getAllTransactions_success() throws Exception {
        var response = UseCaseResponse.<List<TransactionDto>>builder().result(List.of()).build();

        when(getAllTransactionUseCase.call()).thenReturn(response);

        mvc.perform(get("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(0)))
                .andReturn();
    }
}
