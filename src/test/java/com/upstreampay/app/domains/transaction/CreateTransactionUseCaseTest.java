package com.upstreampay.app.domains.transaction;

import com.upstreampay.app.commons.ErrorMessage;
import com.upstreampay.app.commons.UseCaseError;
import com.upstreampay.app.domains.order.OrderDto;
import com.upstreampay.app.persistence.transaction.PaymentMethod;
import com.upstreampay.app.persistence.transaction.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class CreateTransactionUseCaseTest {

    @Autowired
    private CreateTransactionUseCase createTransactionUseCase;

    @Autowired
    private TransactionService transactionService;

	@Test
	void success() {
        var transactionDto = TransactionDto.builder()
                        .amount(BigDecimal.valueOf(4.50))
                        .paymentMethod(PaymentMethod.CREDIT_CARD)
                        .orders(List.of(OrderDto.builder()
                                        .name("Crayon")
                                        .price(BigDecimal.valueOf(1))
                                        .quantity(3)
                                        .build(),
                                OrderDto.builder()
                                        .name("Gomme")
                                        .price(BigDecimal.valueOf(1.50))
                                        .quantity(1)
                                        .build()
                        ))
                        .build();

        var response = createTransactionUseCase.call(transactionDto);

        assertThat(response.isSuccess()).isTrue();

        var result = response.getResult();
        assertThat(result).isNotNull();

        var optFetchedTransaction = transactionService.findById(UUID.fromString(result));
        assertThat(optFetchedTransaction).isPresent().get();
    }


    @Test
    void failure_transactionDto_malformed() {
        var malformedTransaction = TransactionDto.builder().build();

        var response = createTransactionUseCase.call(malformedTransaction);

        assertThat(response.isFailure()).isTrue();
        var result = response.getErrors();

        assertThat(result).hasSize(3).containsExactlyInAnyOrder(
                UseCaseError.builder().field("paymentMethod").message(ErrorMessage.MUST_NOT_BE_NULL).build(),
                UseCaseError.builder().field("amount").message(ErrorMessage.MUST_NOT_BE_NULL).build(),
                UseCaseError.builder().field("orders").message(ErrorMessage.MUST_NOT_BE_NULL).build()
        );
    }

}
