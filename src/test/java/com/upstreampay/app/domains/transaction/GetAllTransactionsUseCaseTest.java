package com.upstreampay.app.domains.transaction;

import com.upstreampay.app.persistence.order.Order;
import com.upstreampay.app.persistence.transaction.PaymentMethod;
import com.upstreampay.app.persistence.transaction.Transaction;
import com.upstreampay.app.persistence.transaction.TransactionService;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class GetAllTransactionsUseCaseTest {

    @Autowired
    private GetAllTransactionUseCase getAllTransactionUseCase;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private ModelMapper modelMapper;

	@Test
	void success() {
        var transaction = Transaction.builder()
                .amount(BigDecimal.valueOf(2.97))
                .paymentMethod(PaymentMethod.CREDIT_CARD)
                .orders(List.of(Order.builder()
                        .name("Crayon")
                        .price(BigDecimal.valueOf(0.99))
                        .quantity(3)
                        .build()))
                .build();
        transaction = transactionService.save(transaction);


        var response = getAllTransactionUseCase.call();

        assertThat(response.isSuccess()).isTrue();

        assertThat(response.getResult()).hasSize(1)
                .containsExactly(modelMapper.map(transaction, TransactionDto.class));
    }


    @Test
    void success_void_list() {
        var response = getAllTransactionUseCase.call();

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getResult()).hasSize(0);
    }

}
