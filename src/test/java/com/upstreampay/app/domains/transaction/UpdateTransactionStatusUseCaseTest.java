package com.upstreampay.app.domains.transaction;

import com.upstreampay.app.commons.ErrorMessage;
import com.upstreampay.app.commons.UseCaseError;
import com.upstreampay.app.domains.order.OrderDto;
import com.upstreampay.app.persistence.order.Order;
import com.upstreampay.app.persistence.transaction.PaymentMethod;
import com.upstreampay.app.persistence.transaction.Transaction;
import com.upstreampay.app.persistence.transaction.TransactionService;
import com.upstreampay.app.persistence.transaction.TransactionStatus;
import org.aspectj.weaver.ast.Or;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Transactional
class UpdateTransactionStatusUseCaseTest {

    @Autowired
    private UpdateTransactionStatusUseCase updateTransactionStatusUseCase;

    @Autowired
    private TransactionService transactionService;

	@Test
	void success_new_to_authorized_status() {
        var transaction = transactionBuilder();
        var dto = UpdateTransactionStatusDto.builder().status(TransactionStatus.AUTHORIZED).build();

        var response = updateTransactionStatusUseCase.call(transaction.getId(), dto);
        assertThat(response.isSuccess()).isTrue();

        var optFetchedTransaction = transactionService.findById(transaction.getId());
        assertThat(optFetchedTransaction).isPresent().get().extracting(Transaction::getStatus).isEqualTo(TransactionStatus.AUTHORIZED);
    }

    @Test
    void success_authorized_to_captured_status() {
        var transaction = transactionBuilder();
        transaction.setStatus(TransactionStatus.AUTHORIZED);
        transaction = transactionService.save(transaction);

        var dto = UpdateTransactionStatusDto.builder().status(TransactionStatus.CAPTURED).build();

        var response = updateTransactionStatusUseCase.call(transaction.getId(), dto);
        assertThat(response.isSuccess()).isTrue();

        var optFetchedTransaction = transactionService.findById(transaction.getId());
        assertThat(optFetchedTransaction).isPresent().get().extracting(Transaction::getStatus).isEqualTo(TransactionStatus.CAPTURED);
    }

    @Test
    void failure_new_to_captured_status() {
        var transaction = transactionBuilder();

        var dto = UpdateTransactionStatusDto.builder().status(TransactionStatus.CAPTURED).build();

        var response = updateTransactionStatusUseCase.call(transaction.getId(), dto);
        assertThat(response.isFailure()).isTrue();

        assertThat(response.getErrors()).hasSize(1).containsExactly(
                UseCaseError.builder().field("action").message(ErrorMessage.FORBIDDEN_ON_USER).build());
    }


    @Test
    void failure_transactionDto_malformed() {
        var transaction = transactionBuilder();

        var malformedTransaction = UpdateTransactionStatusDto.builder().build();

        var response = updateTransactionStatusUseCase.call(transaction.getId(), malformedTransaction);

        assertThat(response.isFailure()).isTrue();
        var result = response.getErrors();

        assertThat(result).hasSize(1).containsExactly(
                UseCaseError.builder().field("status").message(ErrorMessage.MUST_NOT_BE_NULL).build());
    }


    @Test
    void failure_transaction_not_found() {
        var randomTransactionId = UUID.randomUUID();

        var dto = UpdateTransactionStatusDto.builder().build();

        var response = updateTransactionStatusUseCase.call(randomTransactionId, dto);

        assertTrue(response.isFailure());
        var result = response.getErrors();

        assertThat(result).hasSize(1).containsExactly(
                UseCaseError.builder().field("status").message(ErrorMessage.MUST_NOT_BE_NULL).build());
    }

    private Transaction transactionBuilder(){
        var transaction = Transaction.builder()
                .amount(BigDecimal.valueOf(100))
                .paymentMethod(PaymentMethod.CREDIT_CARD)
                .orders(new ArrayList<>(Arrays.asList(Order.builder()
                        .name("Crayon")
                        .price(BigDecimal.valueOf(0.99))
                        .quantity(3)
                        .build())))
                .build();

        return transactionService.save(transaction);
    }

}
